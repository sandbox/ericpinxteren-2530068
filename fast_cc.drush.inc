<?php
/**
 * @file
 * Drush commands.
 */

/**
 * Implements hook_drush_command().
 */
function fast_cc_drush_command() {
  $items = array();
  $items['fast-cc'] = array(
    'description' => 'Fast clear cache',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
    'aliases' => array('fcc'),
  );
  return $items;
}

/**
 * Drush dispatch wrapper.
 */
function drush_fast_cc() {
  _fast_cc_execute_cache_handler_with_timed_log("All cache cleared", "_fast_cc_clear_all_cache");
}

/**
 * Call all clear cache handlers.
 */
function _fast_cc_clear_all_cache() {
  _fast_cc_execute_cache_handler_with_timed_log("cache tables truncated", "fast_cc_truncate_cache_tables");
  _fast_cc_execute_cache_handler_with_timed_log("cache files deleted", "fast_cc_delete_cache_files");
  if (function_exists("dmemcache_stats")) {
    _fast_cc_execute_cache_handler_with_timed_log("entries invalidated from memcache", "fact_cc_flush_memcache");
  }
}

/**
 * Clear cache by handler, print the caches cleared and time elapsed.
 *
 * @param string $description
 *   The description of the cache clear handler.
 * @param string $function_callback
 *   The function to clear the cache by,
 *   can optionally return a number of the caches cleared.
 */
function _fast_cc_execute_cache_handler_with_timed_log($description, $function_callback) {
  $timer = microtime(TRUE);
  $count = $function_callback();
  $timer = round((microtime(TRUE) - $timer) * 1000, 0);

  if ($count !== NULL) {
    $description = "$count $description";
  }

  drush_log(sprintf("%40s in %3sms", $description, $timer), "ok");
}
