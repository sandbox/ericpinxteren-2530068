This module is only for your development environment. 

!!!!!!! SO DON'T USE FOR PRODUCTION !!!!!!!!!!

This module will clears the cache fast for developers. No hooks will be called.

* It will truncate all tables with 'cache' in it's name
* Remove all cached js/css/language files
* Invalidates memcached entries


